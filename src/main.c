#include <stdio.h>
#include "biblioteca_cobranca.h"

int main(){
    int hora_inicial, minuto_inicial, hora_final, minuto_final;
    float valor_normal, custo_total;
    
    printf("Digite a hora e minuto inicial da ligacao (formato hh:mm): ");
    scanf("%d:%d", &hora_inicial, &minuto_inicial);
    
    printf("Digite a hora e minuto final da ligacao (formato hh:mm): ");
    scanf("%d:%d", &hora_final, &minuto_final);
    
    printf("Digite o valor normal do minuto da ligacao: ");
    scanf("%f", &valor_normal);
    
    custo_total = calcular_custo_ligacao(hora_inicial, minuto_inicial, hora_final, minuto_final, valor_normal);
    
    printf("O custo total da ligacao eh: R$ %.2f\n", custo_total);
    
    return 0;
}
