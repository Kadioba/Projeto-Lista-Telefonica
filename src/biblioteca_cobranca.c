int main() {
#include "biblioteca_cobranca.h"

#include <stdio.h>

float calcular_valor_minuto(int hora, int minuto, float valor_normal){
    float valor_final = valor_normal;
    
    if(hora >= 0 && hora <= 9){
        valor_final = valor_normal * 0.5;
    } else if(hora >= 18 && hora <= 21){
        valor_final = valor_normal * 0.7;
    } else if(hora >= 22 && hora <= 23){
        valor_final = valor_normal * 0.6;
    } else if(hora >= 21 && hora <= 22){
        valor_final = valor_normal * 0.4;
    }
    
    return valor_final;
}

float calcular_custo_ligacao(int hora_inicial, int minuto_inicial, int hora_final, int minuto_final, float valor_normal){
    int total_minutos = (hora_final - hora_inicial) * 60 + (minuto_final - minuto_inicial);
    float custo_total = 0;
    
    while(total_minutos > 0){
        float valor_minuto = calcular_valor_minuto(hora_inicial, minuto_inicial, valor_normal);
        custo_total += valor_minuto;
        total_minutos--;
        
        if(minuto_inicial == 59){
            minuto_inicial = 0;
            hora_inicial++;
        } else {
            minuto_inicial++;
        }
    }
    
    return custo_total;
}

return 0;
}
